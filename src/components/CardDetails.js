import React, { useState, useEffect } from 'react'
import Skeleton from 'react-loading-skeleton';
import { getCardDetails,disableCard } from '../api'
import StatusBadge from './StatusBadge'
import './TransactionList.css'

const CardDetails = ({selectedCard,selectedCardId}) => {
  const [card, setCard] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  useEffect(() => {
      setIsLoading(true)
      getCardDetails(selectedCardId)
        .then(json => {
          setIsLoading(false)
          setCard(json)
        });
    }, [selectedCardId]);
    function disableCardAction(){
      
      var r = window.confirm("Do you really want to disable this card?");
      if (r) {
        disableCard(selectedCardId).then(json=>{
          selectedCard = json; //Not working ==> this should go to another place to affect the selected card to change the active value
        })
      }
    }
  if (isLoading || card === undefined) {
    return (
      <div className="CardDetails">
        <Skeleton
          className="CardDetails__placeholderItem"
          height={'70px'}
          count={4}
        />
      </div>
    )
  }

  return (
    <div
      className="CardDetails__card"
    >
      <div className="CardDetails__statusContainer">
        <StatusBadge
          active={selectedCard.active}
          disabled={!selectedCard.active}
        />
        {selectedCard.active?<div onClick={()=>disableCardAction()} className="cardDisabler">X</div>:<React.Fragment></React.Fragment>}
      </div>
      <div
        className="CardDetails__cardBox lightText"
      >
        <div className="CardDetails__name">
          <b>{selectedCard.name}</b>
        </div>
        <div className="CardDetails__number">
          <b>{card.number}</b>
        </div>
        <div className="CardDetails__exp">
          Exp <b>{card.expiration_month}</b>/<b>{card.expiration_year}</b>
        </div>
        <div className="CardDetails__cvv">
          CVV <b>{card.cvv}</b>
        </div>
        <div className="CardDetails__network">
          <b>{selectedCard.network}</b>
        </div>
      </div>
    </div>
  )
}

export default CardDetails