import React, { useState, useEffect } from 'react'
import Skeleton from 'react-loading-skeleton';
import { getCardTransactions } from '../api'

import './CardDetails.css'

const TransactionList = ({selectedCard}) => {
  const [transactions, setTransactions] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [search, setSearch] = useState("")
  useEffect(() => {
      setIsLoading(true)
      getCardTransactions(selectedCard)
        .then(json => {
          setIsLoading(false)
          setTransactions(json)
          setSearch("");
          console.log(json);
        });
    }, [selectedCard]);
  
  if (isLoading) {
    return (
      <div className="TransactionList">
        <p className="subheaderText">Transactions</p>
        <Skeleton
          className="TransactionList__placeholderItem"
          height={'70px'}
          count={4}
        />
      </div>
    )
  }
   
  return (
    <div className="TransactionList">
      <p className="subheaderText">Transactions</p>
      <input type="text" name="search" onChange={(e)=>setSearch(e.target.value)} className="TransactionList__search" placeholder="Search for a transaction"/>
      {/* const regex = ;{transactions.map */}
      {transactions.filter(function(t) { return (t.label.search(new RegExp(search, 'i')) > -1)}).map(transaction => {
        const purchaseDate = new Date(transaction.purchase_date)
        const dateString = purchaseDate.toLocaleDateString('en-US')
        return (
          <div key={transaction.id} className="TransactionList__item">
            <div className="TransactionList__itemDate">
              {dateString}
            </div>
            <div className="TransactionList__itemLabel">
              {transaction.label}
            </div>
            <div  className="TransactionList__itemAmount">
              {`- ${transaction.amount}`}
            </div>
          </div>
        )
      })}
    </div>
  )
}

export default TransactionList
