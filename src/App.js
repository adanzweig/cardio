import React, { useState, useEffect } from 'react'
import CardsList from './components/CardsList'
import { getCards } from './api'
import CardDetails from './components/CardDetails'
import TransactionList from './components/TransactionList'
import './App.css'

function App() {
  const [selectedCardId, setSelectedCardId] = useState()
  const [cards, setCards] = useState([])
  const [isLoadingCards, setIsLoadingCards] = useState(false)
  
  useEffect(() => {
    setIsLoadingCards(true)
    getCards()
      .then(json => {
        setIsLoadingCards(false)
        setCards(json)
      });
      
  }, []);
  const selectedCard = cards.find(
    card => card.id === selectedCardId
  )
  return (
    <div>
      <div className="App__nav">
          Card.io
        </div>
      <div className={"content_"+(selectedCardId?'flex':'')}>
        <CardsList 
          className="cardsListBlock"
          cards={cards}
          selectedCardId={selectedCardId}
          onClick={(id) => setSelectedCardId(id)}
          isLoading={isLoadingCards}
        />
          {(selectedCardId !== undefined)?
            <div className="CardDetailBlock">
              <div className="closeBlock" onClick={()=>setSelectedCardId(undefined)}>X</div>
              <CardDetails selectedCardId={selectedCardId} selectedCard={selectedCard} />
              <TransactionList selectedCard={selectedCardId} />
            </div>:<React.Fragment></React.Fragment>}
          </div>
    </div>
  );
}

export default App;
